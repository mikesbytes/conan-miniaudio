from conans import ConanFile, tools
import os

class MiniaudioConan(ConanFile):
    name = "miniaudio"
    license = "https://github.com/mackron/miniaudio/blob/master/LICENSE"
    url = "https://github.com/mackron/miniaudio"
    no_copy_source = True

    scm = {
            "type": "git",
            "subfolder": "miniaudio",
            "url": "https://github.com/mackron/miniaudio.git",
            "revision": "master"
         }

    version = "git"

    #def source(self):
    #    self.run("git clone https://github.com/mackron/miniaudio.git")

    def package(self):
        self.copy("*.h", dst="include/miniaudio", src="miniaudio")
